Received: from DNCDAG1.dnc.org ([fe80::f85f:3b98:e405:6ebe]) by
 dnchubcas2.dnc.org ([::1]) with mapi id 14.03.0224.002; Mon, 2 May 2016
 09:35:58 -0400
From: "Freundlich, Christina" <FreundlichC@dnc.org>
To: Comm_D <Comm_D@dnc.org>
Subject: Politico: Senate on pace for lightest work schedule in 60 years
Thread-Topic: Politico: Senate on pace for lightest work schedule in 60 years
Thread-Index: AdGkd3WY7RWztQHlTZKQttO0c6lkKA==
Date: Mon, 2 May 2016 06:35:57 -0700
Message-ID: <AAEA4E36C4D7A2449432CA66AA173899545E8C8B@dncdag1.dnc.org>
Accept-Language: en-US
Content-Language: en-US
X-MS-Exchange-Organization-AuthAs: Internal
X-MS-Exchange-Organization-AuthMechanism: 04
X-MS-Exchange-Organization-AuthSource: dnchubcas2.dnc.org
X-MS-Has-Attach:
X-Auto-Response-Suppress: DR, OOF, AutoReply
X-MS-Exchange-Organization-SCL: -1
X-MS-TNEF-Correlator:
x-originating-ip: [192.168.185.18]
Content-Type: multipart/alternative;
	boundary="_000_AAEA4E36C4D7A2449432CA66AA173899545E8C8Bdncdag1dncorg_"
MIME-Version: 1.0

--_000_AAEA4E36C4D7A2449432CA66AA173899545E8C8Bdncdag1dncorg_
Content-Type: text/plain; charset="us-ascii"

Senate on pace for lightest work schedule in 60 years<http://www.politico.com/story/2016/05/senate-light-work-schedule-222460#ixzz47UQAiKXK>

But Republicans aren't backing down from the election-year message that the chamber is 'back to work.'
By BURGESS EVERETT and SEUNG MIN KIM 05/02/16 05:18 AM EDT

Senate Republicans have left town for another recess with their yearlong claim that the Senate is "back to work" an increasingly tough sell to voters.

Majority Leader Mitch McConnell has worked painstakingly to craft an identity that's distinct from the raucous presidential contest - one built on stability and passage of legislation the Democrats couldn't get through when they controlled the Senate.


But the chamber is on pace to work the fewest days in 60 years, the party continues to insist it won't act on President Barack Obama's Supreme Court nomination, and Republicans' ballyhooed strategy to shepherd all dozen spending bills through the chamber is in serious trouble.

The level of productivity isn't just about bragging rights: Senate Republicans are staking their push to maintain control of the chamber in November on a message that a do-nothing Senate is a thing of the past. Successfully prosecuting that case could become even more critical if Donald Trump, who many believe will be a drag on the GOP ticket, wins the nomination.

Republicans have notched some unambiguous wins. They can brag about opening up the amendment process, along with rewriting of transportation, Medicare and energy laws. But unless Republicans can get the appropriations process back on track and start putting in serious Senate overtime - i.e. working on Fridays - their message could be drowned out. Their refusal to take up Merrick Garland's nomination to the Supreme Court has allowed Democrats to endlessly repeat the line, "Do your job," further undermining their message of a newly prolific Senate.

There is also a bipartisan deal on criminal justice reform legislation, though McConnell has not committed to bring the compromise to the Senate floor.

The debate over whether this Senate is more productive than the last - and who is responsible - is a question that's constantly being litigated by McConnell and Senate Minority Leader Harry Reid. The two are locked in a heated debate over why the first spending bill went down in flames.

Democrats say that between the Supreme Court, missed legislative opportunities and the relatively lax 2016 schedule, they have neutralized the GOP's refrain that the Senate is working again.

"If it was really working, we'd already have a Supreme Court person in here, being interviewed, have a hearing on 'em and potentially have a vote in here this summer. And they don't want to do it," said Sen. Jon Tester of Montana, chairman of Democrats' campaign arm. "Let's get real here. The people that are making the claim that we're working are the same ones who brought everything to a grinding halt" during the last Congress.

Republicans are making one last-ditch effort to claim the productivity mantle. McConnell is forging ahead with a months-long effort to approve the 12 spending bills that fund the government, with plans to use the threat of Friday sessions and late Thursday votes to finish a process crucial to getting the Senate back to "regular order."

But those intentions were laid to waste last week as Democrats filibustered an energy and water funding bill twice over what they viewed as a "poison pill" amendment involving Iran, a move which carries risk for senators in both parties while they are home for a 10-day recess. Republicans may not be making progress on their central goal, but Democrats are now vulnerable to being blamed for obstruction and avoiding a tough vote.

"We're ready to go. We could've stopped or slowed this bill a dozen different ways. But we never did," said Senate Minority Whip Dick Durbin (D-Ill.). "It came down to a controversial last-minute amendment. If that amendment went away, this bill would pass in a heartbeat."

Democrats have long claimed to be a "good" minority, using the filibuster only when needed. And there have been far fewer filibusters during their time in the minority than during the last Congress. But parliamentary breakdowns can be blamed on everyone.

"I hope this doesn't amount to an indication they will do anything and everything they can to stop the appropriations process while claiming they are really interested in [pursuing] it," said Senate Majority Whip John Cornyn of Texas.

Republicans can point to several accomplishments, including a long-term highway law, a permanent fix for Medicare payments and expiring tax breaks, a broad education rewrite and a host of small-bore bills that passed with overwhelming support. They've passed more than 200 bills and held more than 200 amendments.

But the GOP has opened itself to attack after criticizing Democrats for developing a shortened Monday through Thursday schedule that McConnell has largely preserved.

"The first thing I need to do is to get the Senate back to normal. That means working more. I don't think we've had any votes on Friday in anybody's memory," McConnell said immediately after winning reelection and the Senate majority in November 2014.

The Senate still comes in on Monday evening and leaves early Thursday afternoon in a typical week. There's not been a single Friday vote this year.

In 2015, the Senate did work far more days than the Reid-controlled Senate of 2013 and 2014. But the chamber is on pace this year to be in session for fewer days than any year since 1956. Part of that is a quirk of the schedule caused by the party conventions taking in place in July rather than August. But there have also been just three Friday sessions this year and this year's summer recess is the longest in 60 years.

The Senate was scheduled to be in session only 149 days this year, but all of the four-day workweeks have put the chamber on track to be in for just 124 days. That will only change if McConnell begins scheduling more Friday sessions.

"Fewest days since 1956. Listen, he doesn't have much to crow about," Reid said of McConnell.

Republicans argue that the number of Fridays worked has little to do with productivity. They say they are simply more efficient than Democrats.

"More than the amount of time, it's what do you do with the time," Cornyn said.

The Senate schedule is a difficult balance for any leader trying to defend a majority. In 2014, Reid kept the Senate in just 136 days, the least in a decade. Vulnerable senators want to be back home campaigning for reelection and raising money, not in session from Monday morning until Friday evening on spending bills the House probably can't even pass.

Few up for reelection are asking for McConnell to schedule more time in.

"The measure should be what we get done, not how much time here," said Sen. Roy Blunt of Missouri, who is up for reelection. "Members of the Senate and House don't go home to relax."

Even so, others wish they were working more - even Republicans.

Though careful not to criticize the majority leader directly, some Republicans - particularly freshmen - wish the Senate had been more ambitious. Sen. David Perdue (R-Ga.) praised passage of a bill approving the Keystone pipeline and a law to force the Obama administration to submit the Iran nuclear deal to congressional review.

But "what we didn't do is deal with some of the priorities people back home want us to," Perdue said, such the national debt and entitlement reforms. "When I go home ... people are saying, 'Yeah, you did some things, but what about these big issues?'"

Sen. Jeff Sessions (R-Ala.) noted that many of the accomplishments were must-do, "bread-and-butter" Senate work. McConnell has refrained from launching a steady stream of sure-to-fail political votes, a strategy Democrats used effectively in 2012, but with little success in 2014.

"I hear from a lot of people - a complaint and concern that I think is valid - that we need to bring up some legislation that we believe in, even though it may be opposed on the other side," Sessions said. "At least show what we are fighting for."

Gripes aside, the Supreme Court has upended all expectations of the Senate's business this year. Democrats were making a complicated "yes, but" rebuttal to the GOP's productivity argument until Supreme Court Justice Antonin Scalia's death: Sure, the Senate was getting stuff done, but only because Democrats were such a cooperative minority.

Now that the Republicans are dug in against Garland, Democrats say the GOP's claims of productivity ring hollow.

"As long as they continue to block a hearing on the Supreme Court the American people will think, correctly, that they're not doing their job. And it won't help them get well," said Sen. Chuck Schumer of New York, the presumptive Democratic leader after Reid retires.

But Republicans argue that blocking nominee Merrick Garland has, ironically, made the Senate more productive. A source close to McConnell said without Garland tying up the Senate floor, the chamber is making far more progress on legislation than it would if the Senate had a protracted fight over the nomination.

Democrats scoff at that logic, accusing GOP leaders of ignoring everything from funding to combat the Zika virus to a criminal justice bill that has backing from many rank-and-file Republicans. Republicans have also failed to pass a budget after needling Democrats for failing to do so when they were in power.

"It'd be nice to have a year when we actually did things, and this is something that we could do," lamented Sen. Patrick Leahy (D-Vt.), who has been working on the criminal justice bill. "I know we have a number of Republicans and a number of Democrats who have been working very, very hard. I'd just like to maybe take a few less recesses and vacations."



Read more: http://www.politico.com/story/2016/05/senate-light-work-schedule-222460#ixzz47VO5aSTs
Follow us: @politico on Twitter | Politico on Facebook

--_000_AAEA4E36C4D7A2449432CA66AA173899545E8C8Bdncdag1dncorg_
Content-Type: text/html; charset="us-ascii"

<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=us-ascii">
<meta name="Generator" content="Microsoft Word 14 (filtered medium)">
<style><!--
/* Font Definitions */
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Calibri","sans-serif";
	color:windowtext;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri","sans-serif";}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]-->
</head>
<body lang="EN-US" link="blue" vlink="purple">
<div class="WordSection1">
<p class="MsoNormal"><b><a href="http://www.politico.com/story/2016/05/senate-light-work-schedule-222460#ixzz47UQAiKXK">Senate on pace for lightest work schedule in 60 years</a><o:p></o:p></b></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">But Republicans aren't backing down from the election-year message that the chamber is 'back to work.'<o:p></o:p></p>
<p class="MsoNormal">By BURGESS EVERETT and SEUNG MIN KIM 05/02/16 05:18 AM EDT<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Senate Republicans have left town for another recess with their yearlong claim that the Senate is &quot;back to work&quot; an increasingly tough sell to voters.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Majority Leader Mitch McConnell has worked painstakingly to craft an identity that's distinct from the raucous presidential contest &#8212; one built on stability and passage of legislation the Democrats couldn't get through when they controlled
 the Senate.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">But the chamber is on pace to work the fewest days in 60 years, the party continues to insist it won't act on President Barack Obama's Supreme Court nomination, and Republicans' ballyhooed strategy to shepherd all dozen spending bills through
 the chamber is in serious trouble.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">The level of productivity isn't just about bragging rights: Senate Republicans are staking their push to maintain control of the chamber in November on a message that a do-nothing Senate is a thing of the past. Successfully prosecuting
 that case could become even more critical if Donald Trump, who many believe will be a drag on the GOP ticket, wins the nomination.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Republicans have notched some unambiguous wins. They can brag about opening up the amendment process, along with rewriting of transportation, Medicare and energy laws. But unless Republicans can get the appropriations process back on track
 and start putting in serious Senate overtime &#8212; i.e. working on Fridays &#8212; their message could be drowned out. Their refusal to take up Merrick Garland's nomination to the Supreme Court has allowed Democrats to endlessly repeat the line, &#8220;Do your job,&#8221; further
 undermining their message of a newly prolific Senate.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">There is also a bipartisan deal on criminal justice reform legislation, though McConnell has not committed to bring the compromise to the Senate floor.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">The debate over whether this Senate is more productive than the last &#8212; and who is responsible &#8212; is a question that's constantly being litigated by McConnell and Senate Minority Leader Harry Reid. The two are locked in a heated debate over
 why the first spending bill went down in flames.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Democrats say that between the Supreme Court, missed legislative opportunities and the relatively lax 2016 schedule, they have neutralized the GOP&#8217;s refrain that the Senate is working again.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">&#8220;If it was really working, we&#8217;d already have a Supreme Court person in here, being interviewed, have a hearing on &#8216;em and potentially have a vote in here this summer. And they don&#8217;t want to do it,&#8221; said Sen. Jon Tester of Montana, chairman
 of Democrats&#8217; campaign arm. &#8220;Let&#8217;s get real here. The people that are making the claim that we&#8217;re working are the same ones who brought everything to a grinding halt&#8221; during the last Congress.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Republicans are making one last-ditch effort to claim the productivity mantle. McConnell is forging ahead with a months-long effort to approve the 12 spending bills that fund the government, with plans to use the threat of Friday sessions
 and late Thursday votes to finish a process crucial to getting the Senate back to &quot;regular order.&quot;<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">But those intentions were laid to waste last week as Democrats filibustered an energy and water funding bill twice over what they viewed as a &quot;poison pill&quot; amendment involving Iran, a move which carries risk for senators in both parties
 while they are home for a 10-day recess. Republicans may not be making progress on their central goal, but Democrats are now vulnerable to being blamed for obstruction and avoiding a tough vote.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">&#8220;We&#8217;re ready to go. We could&#8217;ve stopped or slowed this bill a dozen different ways. But we never did,&#8221; said Senate Minority Whip Dick Durbin (D-Ill.). &#8220;It came down to a controversial last-minute amendment. If that amendment went away,
 this bill would pass in a heartbeat.&#8221;<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Democrats have long claimed to be a &quot;good&quot; minority, using the filibuster only when needed. And there have been far fewer filibusters during their time in the minority than during the last Congress. But parliamentary breakdowns can be blamed
 on everyone.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">&quot;I hope this doesn't amount to an indication they will do anything and everything they can to stop the appropriations process while claiming they are really interested in [pursuing] it,&quot; said Senate Majority Whip John Cornyn of Texas.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Republicans can point to several accomplishments, including a long-term highway law, a permanent fix for Medicare payments and expiring tax breaks, a broad education rewrite and a host of small-bore bills that passed with overwhelming support.
 They've passed more than 200 bills and held more than 200 amendments.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">But the GOP has opened itself to attack after criticizing Democrats for developing a shortened Monday through Thursday schedule that McConnell has largely preserved.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">&#8220;The first thing I need to do is to get the Senate back to normal. That means working more. I don't think we've had any votes on Friday in anybody's memory,&#8221; McConnell said immediately after winning reelection and the Senate majority in
 November 2014.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">The Senate still comes in on Monday evening and leaves early Thursday afternoon in a typical week. There&#8217;s not been a single Friday vote this year.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">In 2015, the Senate did work far more days than the Reid-controlled Senate of 2013 and 2014. But the chamber is on pace this year to be in session for fewer days than any year since 1956. Part of that is a quirk of the schedule caused by
 the party conventions taking in place in July rather than August. But there have also been just three Friday sessions this year and this year's summer recess is the longest in 60 years.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">The Senate was scheduled to be in session only 149 days this year, but all of the four-day workweeks have put the chamber on track to be in for just 124 days. That will only change if McConnell begins scheduling more Friday sessions.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">&#8220;Fewest days since 1956. Listen, he doesn&#8217;t have much to crow about,&#8221; Reid said of McConnell.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Republicans argue that the number of Fridays worked has little to do with productivity. They say they are simply more efficient than Democrats.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">&#8220;More than the amount of time, it&#8217;s what do you do with the time,&#8221; Cornyn said.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">The Senate schedule is a difficult balance for any leader trying to defend a majority. In 2014, Reid kept the Senate in just 136 days, the least in a decade. Vulnerable senators want to be back home campaigning for reelection and raising
 money, not in session from Monday morning until Friday evening on spending bills the House probably can&#8217;t even pass.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Few up for reelection are asking for McConnell to schedule more time in.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">&#8220;The measure should be what we get done, not how much time here,&#8221; said Sen. Roy Blunt of Missouri, who is up for reelection. &#8220;Members of the Senate and House don&#8217;t go home to relax.&#8221;<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Even so, others wish they were working more &#8212; even Republicans.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Though careful not to criticize the majority leader directly, some Republicans &#8212; particularly freshmen &#8212; wish the Senate had been more ambitious. Sen. David Perdue (R-Ga.) praised passage of a bill approving the Keystone pipeline and a
 law to force the Obama administration to submit the Iran nuclear deal to congressional review.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">But &#8220;what we didn&#8217;t do is deal with some of the priorities people back home want us to,&#8221; Perdue said, such the national debt and entitlement reforms. &#8220;When I go home ... people are saying, &#8216;Yeah, you did some things, but what about these
 big issues?&#8217;&#8221;<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Sen. Jeff Sessions (R-Ala.) noted that many of the accomplishments were must-do, &#8220;bread-and-butter&#8221; Senate work. McConnell has refrained from launching a steady stream of sure-to-fail political votes, a strategy Democrats used effectively
 in 2012, but with little success in 2014.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">&#8220;I hear from a lot of people &#8212; a complaint and concern that I think is valid &#8212; that we need to bring up some legislation that we believe in, even though it may be opposed on the other side,&#8221; Sessions said. &#8220;At least show what we are fighting
 for.&quot;<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Gripes aside, the Supreme Court has upended all expectations of the Senate's business this year. Democrats were making a complicated &#8220;yes, but&#8221; rebuttal to the GOP&#8217;s productivity argument until Supreme Court Justice Antonin Scalia&#8217;s death:
 Sure, the Senate was getting stuff done, but only because Democrats were such a cooperative minority.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Now that the Republicans are dug in against Garland, Democrats say the GOP&#8217;s claims of productivity ring hollow.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">&quot;As long as they continue to block a hearing on the Supreme Court the American people will think, correctly, that they&#8217;re not doing their job. And it won&#8217;t help them get well,&#8221; said Sen. Chuck Schumer of New York, the presumptive Democratic
 leader after Reid retires.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">But Republicans argue that blocking nominee Merrick Garland has, ironically, made the Senate more productive. A source close to McConnell said without Garland tying up the Senate floor, the chamber is making far more progress on legislation
 than it would if the Senate had a protracted fight over the nomination.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Democrats scoff at that logic, accusing GOP leaders of ignoring everything from funding to combat the Zika virus to a criminal justice bill that has backing from many rank-and-file Republicans. Republicans have also failed to pass a budget
 after needling Democrats for failing to do so when they were in power.<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">&#8220;It&#8217;d be nice to have a year when we actually did things, and this is something that we could do,&#8221; lamented Sen. Patrick Leahy (D-Vt.), who has been working on the criminal justice bill. &#8220;I know we have a number of Republicans and a number
 of Democrats who have been working very, very hard. I&#8217;d just like to maybe take a few less recesses and vacations.&#8221;<o:p></o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal"><o:p>&nbsp;</o:p></p>
<p class="MsoNormal">Read more: http://www.politico.com/story/2016/05/senate-light-work-schedule-222460#ixzz47VO5aSTs
<o:p></o:p></p>
<p class="MsoNormal">Follow us: @politico on Twitter | Politico on Facebook<o:p></o:p></p>
</div>
</body>
</html>

--_000_AAEA4E36C4D7A2449432CA66AA173899545E8C8Bdncdag1dncorg_--
