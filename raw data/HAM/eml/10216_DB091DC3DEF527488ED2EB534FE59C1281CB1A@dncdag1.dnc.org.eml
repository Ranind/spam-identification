Received: from DNCDAG1.dnc.org ([fe80::f85f:3b98:e405:6ebe]) by
 dnchubcas2.dnc.org ([::1]) with mapi id 14.03.0224.002; Fri, 13 May 2016
 15:05:29 -0400
From: "Paustenbach, Mark" <PaustenbachM@dnc.org>
To: Debbie Wasserman Schultz <hrtsleeve@gmail.com>
CC: "Miranda, Luis" <MirandaL@dnc.org>
Subject: AP - 'Self-funded' Donald Trump preparing to seek big-donor money
Thread-Topic: AP - 'Self-funded' Donald Trump preparing to seek big-donor
 money
Thread-Index: AdGtSgVx456MQTTLTaS08Gdkvb8RTw==
Date: Fri, 13 May 2016 12:05:28 -0700
Message-ID: <DB091DC3DEF527488ED2EB534FE59C1281CB1A@dncdag1.dnc.org>
Accept-Language: en-US
Content-Language: en-US
X-MS-Exchange-Organization-AuthAs: Internal
X-MS-Exchange-Organization-AuthMechanism: 04
X-MS-Exchange-Organization-AuthSource: dnchubcas2.dnc.org
X-MS-Has-Attach:
X-MS-Exchange-Organization-SCL: -1
X-MS-TNEF-Correlator:
x-originating-ip: [192.168.177.129]
Content-Type: multipart/alternative;
	boundary="_000_DB091DC3DEF527488ED2EB534FE59C1281CB1Adncdag1dncorg_"
MIME-Version: 1.0

--_000_DB091DC3DEF527488ED2EB534FE59C1281CB1Adncdag1dncorg_
Content-Type: text/plain; charset="us-ascii"

'Self-funded' Donald Trump preparing to seek big-donor money<http://bigstory.ap.org/34f1ee4e267f4bb1a7e9484a12883129>
By JULIE BYKOWICZ<http://bigstory.ap.org/journalist/julie-bykowicz>
May. 13, 2016 1:41 PM EDT


LAS VEGAS (AP) - The billionaire presidential candidate who prides himself on paying his own way and bashed his competition for relying on political donors now wants their money - and lots of it.

Donald Trump, the presumptive Republican presidential nominee, recently hired a national finance chairman, scheduled his first fundraiser and is on the cusp of signing a deal with the Republican Party that would enable him to solicit donations of more than $300,000 apiece from supporters.

His money-raising begins right away.

The still-forming finance team is planning a dialing-for-dollars event on the fifth floor of Trump Tower in New York, and the campaign is at work on a fundraising website focused on small donations. In addition to a May 25 fundraiser at the Los Angeles home of real estate developer Tom Barrack, he'll hold another soon thereafter in New York.

The political newcomer faces a gargantuan task: A general election campaign can easily run up a $1 billion tab. For the primary race, Trump spent a tiny fraction of that amount - he's estimated $50 million of his own money, plus about $12 million from donors who sought his campaign out on their own.

Trump told The Associated Press in an interview this week that he will spend minimally on a data operation that can help identify and turn out voters. And he's betting that the media's coverage of his rallies and celebrity personality will reduce his need for pricey television advertising.

Yet he acknowledged that the general-election campaign may cost "a lot." To help raise the needed money, he tapped Steven Mnuchin, a New York investor with ties in Hollywood and Las Vegas but no political fundraising experience.

"To me this is no different than building a business, and this is a business with a fabulous product: Donald Trump," Mnuchin said in an interview at a financial industry conference in Las Vegas. Trump's new national finance chairman said prospective donors are "coming out of the woodwork" and he's been fielding emails and phone calls from people he hasn't heard from in 20 years.

More experienced fundraisers are coming aboard, too, such as Eli Miller of Washington, Anthony Scaramucci of New York and Ray Washburn of Dallas. All three helped raise money for candidates Trump defeated in the primary.

To convey the amount of work needed to vacuum up money, Scaramucci, part of 2012 GOP nominee Mitt Romney's finance team, recently shared Romney's old fundraising calendar with Trump. He said Trump was receptive to a schedule that has 50 to 100 fundraisers over the summer.

Scaramucci said he didn't expect Trump to grovel for donors. "But is he going to say thank you and be appreciative? Of course. He's very good one-on-one. He's a hard guy not to like."

Trump's dilemma: By asking for money, he could anger supporters who love his assertion that he's different from most politicians because he isn't beholden to donors.

He's tried to navigate these tricky waters by saying he wants only to raise money to benefit the party and help elect other Republicans. But his planned joint fundraising agreement with Republican officials also provides a direct route to his own campaign coffers.

Such an arrangement could work like this: For each large contribution, the first $2,700 or $5,400 goes to Trump's campaign, the next $33,400 goes to the Republican National Committee, similar amounts could go to national party accounts and the rest is divided evenly among various state parties the candidate selects.

Democrat Hillary Clinton set up such a victory committee in September, and it had collected $61 million by the end of March.

She also counts on several super PACs that. They've landed million-dollar checks from her friends and supporters and already scheduled $130 million in TV, radio and internet ads leading up to Election Day.

Trump is only now beginning to turn his attention to this kind of big money. A decision on how fully to embrace outside groups is fraught with possible charges of hypocrisy, since he has called them "corrupt."

Still, wealthy Trump supporters have several options - and megadonors are beginning to line up.

Sheldon Adelson, a billionaire Las Vegas casino owner who was the largest donor of the 2012 presidential race, wrote in a Washington Post editorial this week that he endorses Trump and is urging "those who provide important financial backing" to do the same.

Libertarian billionaire Peter Thiel, who co-founded PayPal and pumped millions of dollars into Ron Paul's presidential bid four years ago, recently signed on as a California delegate for Trump. And billionaire oil investor T. Boone Pickens said this week he intends to help finance Trump's effort. He's invited officials from one of the pro-Trump super PACs to his Texas ranch next month.

That entity, Great America PAC, has struggled to get off the ground but hopes to raise $15 million to $20 million in the next few months, said its chief fundraiser, Eric Beach. The group recently brought on Ronald Reagan's campaign manager Ed Rollins, whom Trump has praised.

On Thursday, another pro-Trump super PAC emerged. Doug Watts, former communications director for Ben Carson's 2016 bid, started a group called the Committee for American Sovereignty with an advisory board that include former Trump resorts executive Nicholas Ribis Sr. and longtime GOP donor Kenneth Abramowitz. The group aims to raise $20 million before the GOP convention in July.

One Trump emissary to the world of major donors is billionaire investor Carl Icahn, who made calls to Pickens and others to gauge their interest in Trump.

Some are biting, either because of support for Trump or a desire to keep Clinton out of office. Among the latter group is Stanley Hubbard, a Minnesota broadcast billionaire who spent money trying to "stop Trump."

Having failed in that quest, he said he's prepared to write a check to stop Clinton.

___

Associated Press writer Jill Colvin contributed to this report.


Mark Paustenbach
National Press Secretary &
Deputy Communications Director
Democratic National Committee
W: 202.863.8148
paustenbachm@dnc.org<mailto:paustenbachm@dnc.org>


--_000_DB091DC3DEF527488ED2EB534FE59C1281CB1Adncdag1dncorg_
Content-Type: text/html; charset="us-ascii"

<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=us-ascii">
<meta name="Generator" content="Microsoft Word 14 (filtered medium)">
<style><!--
/* Font Definitions */
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Palatino Linotype";
	panose-1:2 4 5 2 5 5 5 3 3 4;}
/* Style Definitions */
p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
h1
	{mso-style-priority:9;
	mso-style-link:"Heading 1 Char";
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	font-size:24.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
a:link, span.MsoHyperlink
	{mso-style-priority:99;
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-priority:99;
	color:purple;
	text-decoration:underline;}
p
	{mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
span.EmailStyle17
	{mso-style-type:personal-compose;
	font-family:"Palatino Linotype","serif";
	color:windowtext;}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-priority:9;
	mso-style-link:"Heading 1";
	font-family:"Times New Roman","serif";
	font-weight:bold;}
span.apple-converted-space
	{mso-style-name:apple-converted-space;}
span.updated
	{mso-style-name:updated;}
.MsoChpDefault
	{mso-style-type:export-only;
	font-family:"Calibri","sans-serif";}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
--></style><!--[if gte mso 9]><xml>
<o:shapedefaults v:ext="edit" spidmax="1026" />
</xml><![endif]--><!--[if gte mso 9]><xml>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1" />
</o:shapelayout></xml><![endif]-->
</head>
<body lang="EN-US" link="blue" vlink="purple">
<div class="WordSection1">
<h1 style="mso-margin-top-alt:6.0pt;margin-right:0in;margin-bottom:6.0pt;margin-left:0in;line-height:13.1pt;background:white">
<span style="font-size:12.0pt"><a href="http://bigstory.ap.org/34f1ee4e267f4bb1a7e9484a12883129"><span style="color:windowtext">'Self-funded' Donald Trump preparing to seek big-donor money</span></a><o:p></o:p></span></h1>
<p class="MsoNormal" style="line-height:21.6pt;background:white"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">By<span class="apple-converted-space">&nbsp;</span><a href="http://bigstory.ap.org/journalist/julie-bykowicz"><span style="color:windowtext">JULIE
 BYKOWICZ</span></a><o:p></o:p></span></p>
<p class="MsoNormal" style="line-height:21.6pt;background:white"><span class="updated"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">May. 13, 2016 1:41 PM EDT</span></span><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><o:p></o:p></span></p>
<p class="MsoNormal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt">
LAS VEGAS (AP) &#8212; The billionaire presidential candidate who prides himself on paying his own way and bashed his competition for relying on political donors now wants their money &#8212; and lots of it.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
Donald Trump, the presumptive Republican presidential nominee, recently hired a national finance chairman, scheduled his first fundraiser and is on the cusp of signing a deal with the Republican Party that would enable him to solicit donations of more than
 $300,000 apiece from supporters.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
His money-raising begins right away.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
The still-forming finance team is planning a dialing-for-dollars event on the fifth floor of Trump Tower in New York, and the campaign is at work on a fundraising website focused on small donations. In addition to a May 25 fundraiser at the Los Angeles home
 of real estate developer Tom Barrack, he'll hold another soon thereafter in New York.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt">
The political newcomer faces a gargantuan task: A general election campaign can easily run up a $1 billion tab. For the primary race, Trump spent a tiny fraction of that amount &#8212; he's estimated $50 million of his own money, plus about $12 million from donors
 who sought his campaign out on their own.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
Trump told The Associated Press in an interview this week that he will spend minimally on a data operation that can help identify and turn out voters. And he's betting that the media's coverage of his rallies and celebrity personality will reduce his need for
 pricey television advertising.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
Yet he acknowledged that the general-election campaign may cost &quot;a lot.&quot; To help raise the needed money, he tapped Steven Mnuchin, a New York investor with ties in Hollywood and Las Vegas but no political fundraising experience.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
&quot;To me this is no different than building a business, and this is a business with a fabulous product: Donald Trump,&quot; Mnuchin said in an interview at a financial industry conference in Las Vegas. Trump's new national finance chairman said prospective donors
 are &quot;coming out of the woodwork&quot; and he's been fielding emails and phone calls from people he hasn't heard from in 20 years.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
<span style="background:yellow;mso-highlight:yellow">More experienced fundraisers are coming aboard, too, such as Eli Miller of Washington, Anthony Scaramucci of New York and Ray Washburn of Dallas. All three helped raise money for candidates Trump defeated
 in the primary.</span><o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
To convey the amount of work needed to vacuum up money, Scaramucci, part of 2012 GOP nominee Mitt Romney's finance team, recently shared Romney's old fundraising calendar with Trump.
<span style="background:yellow;mso-highlight:yellow">He said Trump was receptive to a schedule that has 50 to 100 fundraisers over the summer.</span><o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
Scaramucci said he didn't expect Trump to grovel for donors. &quot;But is he going to say thank you and be appreciative? Of course. He's very good one-on-one. He's a hard guy not to like.&quot;<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
<span style="background:yellow;mso-highlight:yellow">Trump's dilemma: By asking for money, he could anger supporters who love his assertion that he's different from most politicians because he isn't beholden to donors.<o:p></o:p></span></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
<span style="background:yellow;mso-highlight:yellow">He's tried to navigate these tricky waters by saying he wants only to raise money to benefit the party and help elect other Republicans. But his planned joint fundraising agreement with Republican officials
 also provides a direct route to his own campaign coffers.</span><o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
Such an arrangement could work like this: For each large contribution, the first $2,700 or $5,400 goes to Trump's campaign, the next $33,400 goes to the Republican National Committee, similar amounts could go to national party accounts and the rest is divided
 evenly among various state parties the candidate selects.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
Democrat Hillary Clinton set up such a victory committee in September, and it had collected $61 million by the end of March.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
She also counts on several super PACs that. They've landed million-dollar checks from her friends and supporters and already scheduled $130 million in TV, radio and internet ads leading up to Election Day.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
<span style="background:yellow;mso-highlight:yellow">Trump is only now beginning to turn his attention to this kind of big money. A decision on how fully to embrace outside groups is fraught with possible charges of hypocrisy, since he has called them &quot;corrupt.&quot;</span><o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
Still, wealthy Trump supporters have several options &#8212; and megadonors are beginning to line up.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
Sheldon Adelson, a billionaire Las Vegas casino owner who was the largest donor of the 2012 presidential race, wrote in a Washington Post editorial this week that he endorses Trump and is urging &quot;those who provide important financial backing&quot; to do the same.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
Libertarian billionaire Peter Thiel, who co-founded PayPal and pumped millions of dollars into Ron Paul's presidential bid four years ago, recently signed on as a California delegate for Trump.
<span style="background:yellow;mso-highlight:yellow">And billionaire oil investor T. Boone Pickens said this week he intends to help finance Trump's effort. He's invited officials from one of the pro-Trump super PACs to his Texas ranch next month.<o:p></o:p></span></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
<span style="background:yellow;mso-highlight:yellow">That entity, Great America PAC, has struggled to get off the ground but hopes to raise $15 million to $20 million in the next few months, said its chief fundraiser, Eric Beach. The group recently brought
 on Ronald Reagan's campaign manager Ed Rollins, whom Trump has praised.</span><o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
On Thursday, another pro-Trump super PAC emerged. Doug Watts, former communications director for Ben Carson's 2016 bid, started a group called the Committee for American Sovereignty with an advisory board that include former Trump resorts executive Nicholas
 Ribis Sr. and longtime GOP donor Kenneth Abramowitz. The group aims to raise $20 million before the GOP convention in July.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
One Trump emissary to the world of major donors is billionaire investor Carl Icahn, who made calls to Pickens and others to gauge their interest in Trump.<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
<span style="background:yellow;mso-highlight:yellow">Some are biting, either because of support for Trump or a desire to keep Clinton out of office. Among the latter group is Stanley Hubbard, a Minnesota broadcast billionaire who spent money trying to &quot;stop
 Trump.&quot;<o:p></o:p></span></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
<span style="background:yellow;mso-highlight:yellow">Having failed in that quest, he said he's prepared to write a check to stop Clinton.</span><o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
___<o:p></o:p></p>
<p style="mso-margin-top-alt:0in;margin-right:0in;margin-bottom:8.25pt;margin-left:0in;line-height:18.0pt;box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
Associated Press writer Jill Colvin contributed to this report.<o:p></o:p></p>
<p class="MsoNormal" style="box-sizing: border-box;font-size:1rem;text-rendering: optimizeLegibility">
<span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p class="MsoNormal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><o:p>&nbsp;</o:p></span></p>
<p class="MsoNormal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Mark Paustenbach<o:p></o:p></span></p>
<p class="MsoNormal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">National Press Secretary &amp;
<br>
Deputy Communications Director<o:p></o:p></span></p>
<p class="MsoNormal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">Democratic National Committee<o:p></o:p></span></p>
<p class="MsoNormal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;">W: 202.863.8148<br>
<a href="mailto:paustenbachm@dnc.org"><span style="color:windowtext">paustenbachm@dnc.org</span></a>&nbsp;
<o:p></o:p></span></p>
<p class="MsoNormal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;"><o:p>&nbsp;</o:p></span></p>
</div>
</body>
</html>

--_000_DB091DC3DEF527488ED2EB534FE59C1281CB1Adncdag1dncorg_--
